##Pre-Receive hook example in response to this [Atlassian Answers Question](https://answers.atlassian.com/questions/39138300/prereceivehook-dependency-injection-problems)

**Important Notes**

- I could only make this work with [SDK 6.2.4](https://marketplace.atlassian.com/plugins/atlassian-plugin-sdk-windows/versions)
- Make sure you enter _pre_ as the hook option when using the SDK
- Add the following annotations:
    - `@Scanned` at the class level
    - `@Inject` at the constructor level
    - `@ComponentImport` on each constructor argument that should be automatically injected
    
Add the following imports:

```java
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;
```