package com.example.hook;

import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import java.util.Collection;

@Scanned
public class MyPreReceiveRepositoryHook implements PreReceiveRepositoryHook
{
    private ContentService contentService;
    private CommitService commitService;

    @Inject
    public MyPreReceiveRepositoryHook(@ComponentImport ContentService contentService, @ComponentImport CommitService commitService){
        this.contentService = contentService;
        this.commitService = commitService;
        System.out.println("constructor successfully called!");
    }

    /**
     * Disables deletion of branches
     */
    @Override
    public boolean onReceive(RepositoryHookContext context, Collection<RefChange> refChanges, HookResponse hookResponse)
    {
        System.out.println("onRecieve called!");

        if (contentService != null)
            System.out.println("ContentService imported!");
        else
            System.out.println("ContentService is null...");

        if(commitService != null)
            System.out.println("CommitService imported!");
        else
            System.out.println("ContentService is null...");

        if(contentService != null && commitService != null)
            System.out.println("SUCCESS!");
        else
            System.out.println("FAIL!");

        // Boilerplate code...
        for (RefChange refChange : refChanges)
        {
            if (refChange.getType() == RefChangeType.DELETE)
            {
                hookResponse.err().println("The ref '" + refChange.getRefId() + "' cannot be deleted.");
                return false;
            }
        }
        return true;
    }
}
